class CalorieEntryDatetimeToDate < ActiveRecord::Migration
  def change
    change_column :calorie_entries, :date, :date
  end
end
