class AddCalorieGoalToUsers < ActiveRecord::Migration
  def change
    add_column :users, :calorie_goal, :integer
  end
end
