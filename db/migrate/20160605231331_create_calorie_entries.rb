class CreateCalorieEntries < ActiveRecord::Migration
  def change
    create_table :calorie_entries do |t|
      t.integer :num_calories, null: false
      t.integer :user_id, null: false
      t.timestamps null: false
    end
  end
end
