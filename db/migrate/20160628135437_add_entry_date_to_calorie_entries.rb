class AddEntryDateToCalorieEntries < ActiveRecord::Migration
  def change
    add_column :calorie_entries, :date, :datetime
    CalorieEntry.find_each do |entry|
      entry.date = entry.created_at
      entry.save!
    end
  end
end
