class CalorieEntriesController < ApplicationController
    before_action :authenticate_user!
    def index
        @calorie_entries = current_user.calorie_entries.order(date: :desc).paginate(:page => params[:page], :per_page => 30)
    end
    
    def show
    end
    
    def new
        @calorie_entry = CalorieEntry.new
        # @calorie_entry.date = Time.now
    end
    
    def create
        if calorie_entry_params[:num_calories].empty? or calorie_entry_params[:date].empty?
            @calorie_entry = CalorieEntry.new
            # @calorie_entry.date = calorie_entry_params[:date].in_time_zone("UTC")
            @calorie_entry.num_calories = calorie_entry_params[:num_calories]
            flash.now[:danger] = "Please fill out all fields."
            render 'new'
        else
            @calorie_entry = CalorieEntry.new(calorie_entry_params)
            @calorie_entry.user = current_user
            if @calorie_entry.save
                flash[:success] = "Entry saved successfully."
                redirect_to root_path
            else
                flash.now[:danger] = "Something went wrong."
                render 'new'
            end
        end
    end
    
    def destroy
        CalorieEntry.find(params[:id]).destroy
        flash[:success] = "Entry deleted."
        redirect_to root_path
    end
    
    private
    def calorie_entry_params
        params.require(:calorie_entry).permit(:num_calories, :user_id, :date)
    end
end