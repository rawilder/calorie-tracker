class ProfileController < ApplicationController
  before_action :authenticate_user!
    
  def index
  end
  
  def edit
  end
    
  def update
    if current_user.update_attributes(profile_params)
    # Handle a successful update.
      redirect_to profile_path
    else
      render 'edit'
    end
  end
  
  def stats
    @num_days = params[:days] ? params[:days].to_f : 7
    @max = 0
    calorie_entries = current_user.calorie_entries_last_x_days(@num_days, :asc)
    @calorie_goal = current_user.calorie_goal
    @calorie_goal_data = {}
    @calorie_data = {}
    calorie_entries.each do |calorie_entry|
      calorie_entry_date = calorie_entry.date.strftime("%b %d, %Y")
      # calorie_entry_date = calorie_entry.date.midnight
      if not @calorie_data[calorie_entry_date]
        @calorie_data[calorie_entry_date] = 0
      end
      @calorie_data[calorie_entry_date] += calorie_entry.num_calories
      @max = @calorie_data[calorie_entry_date] > @max ? @calorie_data[calorie_entry_date] : @max
      @calorie_goal_data[calorie_entry_date] = @calorie_goal
    end
    
  end
    
  private

    def profile_params
      params.require(:user).permit(:calorie_goal, :time_zone)
    end
end
