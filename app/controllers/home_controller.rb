class HomeController < ApplicationController
    before_action :authenticate_user!
    
    def index
        @calorie_entries = current_user.calorie_entries_today.order(date: :desc).paginate(:page => params[:page], :per_page => 10)
    end
    
end
