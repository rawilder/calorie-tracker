class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :calorie_entries
  
  def calories_today
    self.calorie_entries.where("date >= ?", Time.zone.now.to_date).sum(:num_calories)
  end
  
  def calorie_entries_today
    self.calorie_entries.where("date >= ?", Time.zone.now.to_date).order(date: :desc)
  end
  
  def calorie_entries_last_7_days
    self.calorie_entries.where("date >= ?", (Time.zone.now - 7.days).to_date).order(date: :desc)
  end
  
  def calorie_entries_last_x_days(num_days, dir=:desc)
    self.calorie_entries.where("date >= ?", (Time.zone.now - num_days.days).to_date).order(date: dir)
  end
end
